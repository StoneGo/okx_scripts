import json

import okx.Account_api as Account
import okx.Funding_api as Funding
import okx.Market_api as Market
import okx.Public_api as Public
import okx.Trade_api as Trade
import okx.status_api as Status
import okx.subAccount_api as SubAccount
import okx.TradingData_api as TradingData
import okx.Broker_api as Broker
import okx.Convert_api as Convert
import okx.FDBroker_api as FDBroker
import okx.Rfq_api as Rfq
import okx.TradingBot_api as TradingBot

if __name__ == '__main__':
    api_key = "xxx"
    secret_key = "yyy"
    passphrase = "zzz"
    # instrument = "BTC-USDT-SWAP"

    # flag是实盘与模拟盘的切换参数 flag is the key parameter which can help you to change between demo and real trading.
    # flag = '1'  # 模拟盘 demo trading
    flag = '0'  # 实盘 real trading

    insts = ['BTC-USDT','BTC-USD-SWAP', 'BTC-USDT-SWAP']
    # market api
    marketAPI = Market.MarketAPI(api_key, secret_key, passphrase, True, flag)
    # 获取所有产品行情信息  Get Tickers
    # result = marketAPI.get_tickers('SPOT')
    # 获取单个产品行情信息  Get Ticker
    tick = []
    for inst in insts:
        result = marketAPI.get_ticker(inst)
        if result['code'] == '0':
            tick.append(float(result['data'][0]['last']))
    usd_delta = 5
    tick.append(round(tick[1] - tick[0], 2))
    tick.append(round(tick[1] - tick[0] - usd_delta))
    tick.append(round(usd_delta))
    usdt_delta = 10
    tick.append(round(tick[2] - tick[0]))
    tick.append(round(tick[2] - tick[0] - usdt_delta))
    tick.append(round(usdt_delta))
    print(tick)


    # subAccountAPI = SubAccount.SubAccountAPI(api_key, secret_key, passphrase, False, flag)
    # 查询子账户的交易账户余额(适用于母账户) Query detailed balance info of Trading Account of a sub-account via the master account
    # result = subAccountAPI.balances(subAcct='ganluing')
    # print(result)

