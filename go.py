import json

import okx.Account_api as Account
import okx.Funding_api as Funding
import okx.Market_api as Market
import okx.Public_api as Public
import okx.Trade_api as Trade
import okx.status_api as Status
import okx.subAccount_api as SubAccount
import okx.TradingData_api as TradingData
import okx.Broker_api as Broker
import okx.Convert_api as Convert
import okx.FDBroker_api as FDBroker
import okx.Rfq_api as Rfq
import okx.TradingBot_api as TradingBot
import sys
import time


api_key = "xxx"
secret_key = "yyy"
passphrase = "zzz"
instrument = "BTC-USDT-SWAP"


flag = '0'
vol_per_part = 3

def print_usage():
    print('Usage py query_orders.py <cmd> [params]')
    print('  cmd: <pos | order | call | c | jinkong | kaiduo | pingkong | guanduo>')


def print_result(result):
    print('Result Code: ', result['code'])
    for r in result['data']:
        print('    ', r['sCode'], r['sMsg'])


def query_account():
    accountAPI = Account.AccountAPI(api_key, secret_key, passphrase, False, flag)
    result = accountAPI.get_positions('SWAP', instrument)
    # print('positions', json.dumps(result))
    if result['code'] != '0':
        print_result(result)
        return
    print("Positions:")
    for p in result['data']:
        pos = int(p['pos'])
        if pos > 0:
            # print(p)
            # upl = float(p['upl'])
            uplr = float(p['uplRatio']) * 100
            vol = int(int(p['pos']) / vol_per_part)
            aval_vol = int(int(p['availPos']) / vol_per_part)
            print('Side %s -> avgPx %s, vol %d, aval vol %d, uplr %.02lf%% | %s' % 
                    (p['posSide'], p['avgPx'], vol, aval_vol, uplr, p['last']))

def query_market():
    pass
    # market api
    # marketAPI = Market.MarketAPI(api_key, secret_key, passphrase, True, flag)
    # 获取所有产品行情信息  Get Tickers
    # result = marketAPI.get_tickers('SPOT')
    # 获取单个产品行情信息  Get Ticker
    # result = marketAPI.get_ticker('BTC-USDT')
    # 获取指数行情  Get Index Tickers
    # result = marketAPI.get_index_ticker('BTC', 'BTC-USD')
    # 获取产品深度  Get Order Book
    # result = marketAPI.get_orderbook('BTC-USDT-210402', '400')
    # 获取所有交易产品K线数据  Get Candlesticks
    # result = marketAPI.get_candlesticks('BTC-USDT-210924', bar='1m')
    # 获取交易产品历史K线数据（仅主流币实盘数据）  Get Candlesticks History（top currencies in real-trading only）
    # result = marketAPI.get_history_candlesticks('BTC-USDT')
    # 获取指数K线数据  Get Index Candlesticks
    # result = marketAPI.get_index_candlesticks('BTC-USDT')
    # 获取标记价格K线数据  Get Mark Price Candlesticks
    # result = marketAPI.get_markprice_candlesticks('BTC-USDT')
    # 获取交易产品公共成交数据  Get Trades
    # result = marketAPI.get_trades('BTC-USDT', '400')
    # 获取平台24小时成交总量  Get Platform 24 Volume
    # result = marketAPI.get_volume()
    # Oracle 上链交易数据 GET Oracle
    # result = marketAPI.get_oracle()
    # 获取指数成分数据 GET Index Components
    # result = marketAPI.get_index_components(index='')
    # 获取法币汇率 GET exchange rate in legal currency
    # result = marketAPI.get_exchange_rate()
    # 获取交易产品公共历史成交数据
    # result = marketAPI.get_history_trades(instId = 'BTC-USDT', after = '', before = '', limit = '')
    # 获取大宗交易所有产品行情信息
    # result = marketAPI.get_block_tickers(instType = 'SWAP', uly = 'BTC-USDT')
    # 获取大宗交易单个产品行情信息
    # result = marketAPI.get_block_ticker(instId = 'BTC-USDT')
    # 获取大宗交易公共成交数据
    # result = marketAPI.get_block_trades(instId = 'BTC-USDT')



def query_orders():
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    result = tradeAPI.get_order_list()
    if result['code'] == '0':
        for o in result['data']:
            if o['posSide'] == 'short' and o['side'] == 'sell':
                print('Open Short: ', end='')
            elif o['posSide'] == 'long' and o['side'] == 'buy':
                print('Open Long: ', end='')
            elif o['posSide'] == 'short' and o['side'] == 'buy':
                print('Close Short: ', end='')
            elif o['posSide'] == 'long' and o['side'] == 'sell':
                print('Close Long: ', end='')
            sz = int(int(o['sz']) / vol_per_part)
            print(sz, o['px'], o['ordId'])
    else:
        print_result(result)


def cancel_orders(ordId):
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    # 撤单  Cancel Order
    result = tradeAPI.cancel_order(instrument, ordId)
    print_result(result)

def cancel_all_orders():
    # 批量撤单  Cancel Multiple Orders
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    result = tradeAPI.get_order_list()
    if result['code'] == '0':
        orders = [{"instId": instrument, "ordId": o['ordId']} for o in result['data']]
        if len(orders) > 0:
            result = tradeAPI.cancel_multiple_orders(orders)
            print_result(result)
        else:
            print("No order")
    else:
        print_result(result)


def place_orders_open_short(mode, price, vol):
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    # 下单  Place Order
    tz = str(time.time()).replace('.', '')
    real_vol = vol * vol_per_part
    if mode == 'm':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='sell', posSide='short',
                                      ordType='market', sz=real_vol, clOrdId=tz)
    elif mode == 'p':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='sell', posSide='short',
                                      ordType='post_only', px=price, sz=real_vol, clOrdId=tz)
    print_result(result)

def place_orders_close_short(mode, price, vol):
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    # 下单  Place Order
    tz = str(time.time()).replace('.', '')
    real_vol = vol * vol_per_part
    if mode == 'p':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='buy', posSide='short',
                                      ordType='post_only', px=price, sz=real_vol, clOrdId=tz)
    elif mode == 'm':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='buy', posSide='short',
                                      ordType='market', sz=real_vol, clOrdId=tz)
    else:
        print('Mode must be post or market')
    print_result(result)


def place_orders_open_long(mode, price, vol):
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    tz = str(time.time()).replace('.', '')
    real_vol = vol * vol_per_part
    if mode == 'm':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='buy', posSide='long',
                                      ordType='market', sz=real_vol, clOrdId=tz)
    elif mode == 'p':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='buy', posSide='long',
                                      ordType='post_only', px=price, sz=real_vol, clOrdId=tz)
    print_result(result)

def place_orders_close_long(mode, price, vol):
    # trade api
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    # 下单  Place Order
    tz = str(time.time()).replace('.', '')
    real_vol = vol * vol_per_part
    if mode == 'p':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='sell', posSide='long',
                                      ordType='post_only', px=price, sz=real_vol, clOrdId=tz)
    elif mode == 'm':
        result = tradeAPI.place_order(instId=instrument, tdMode='cross', side='sell', posSide='long',
                                      ordType='market', sz=real_vol, clOrdId=tz)
    else:
        print('Mode must be post or market')
    print_result(result)


def query_trade():
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    # 获取成交明细(三天)  Get Transaction Details
    # result = tradeAPI.get_fills()
    # 获取成交明细(三个月)  Get Transaction Details History
    result = tradeAPI.get_fills_history(instType='SWAP')
    # print_result(result)
    print('Code', result['code']);
    if result['code'] == '0':
        data = result['data']
        for d in data:
            print(d)
    else:
        print(result['msg'])


if __name__ == '__main__':
    # print('    <<< VPP  >>> ', vol_per_part)
    if len(sys.argv) < 2:
        # print_usage()
        query_account()
        print("Open Orders: ")
        query_orders()
        exit(0)

    cmd = sys.argv[1]
    if cmd == 'pos':
        query_account()
    elif cmd == 'order':
        query_orders()
    elif cmd == 'call':
        cancel_all_orders()
    elif cmd == 'c':
        if len(sys.argv) != 3:
            print('c <order id>')
        else:
            cancel_orders(sys.argv[2])
    elif cmd == 'jinkong':
        if len(sys.argv) != 5:
            print('jinkong <mode=m|p> <px> <sz>')
        else:
            place_orders_open_short(sys.argv[2], float(sys.argv[3]), int(sys.argv[4]))
    elif cmd == 'kaiduo':
        if len(sys.argv) != 5:
            print('kaiduo <mode=m | p> <px> <sz>')
        else:
            place_orders_open_long(sys.argv[2], float(sys.argv[3]), int(sys.argv[4]))
    elif cmd == 'pingkong':
        if len(sys.argv) != 5:
            print('pingkong <m|p> <px> <sz>')
        else:
            place_orders_close_short(sys.argv[2], float(sys.argv[3]), int(sys.argv[4]))
    elif cmd == 'guanduo':
        if len(sys.argv) != 5:
            print('guanduo <m|p> <px> <sz>')
        else:
            place_orders_close_long(sys.argv[2], float(sys.argv[3]), int(sys.argv[4]))
    elif cmd == 't':
        query_trade()
    else:
        print_usage()

